#include<iostream>
#include<fstream>
#include<vector>
#include<algorithm>
#include<cstdlib>

using namespace std;
unsigned cycles_high,cycles_low,cycles_low1,cycles_high1;


int main(int argc,char* argv[])
{
	//freopen("readings.csv","w","stdout");
	
	const int blockSize = 512;
	int size= 8*atoi(argv[1]); //Size input in megabytes
	int repetitions = atoi(argv[2]);
	int sequential = atoi(argv[3]);
	int noReads = atoi(argv[4]);

	/*
	FILE *fp = fopen("/dev/urandom","r");
	FILE *temp = fopen("/dev/disk2s2","r");
	if(temp == NULL)
	{cout<<"Failed:"<<errno<<endl;}
	setvbuf( temp, (char *)NULL, _IONBF, 0 );
	*/
	int i = size;
	char buf[blockSize];
	/*while (i>0)
	{
			size_t len = fread(buf, 1, sizeof(buf), fp);
			fwrite(buf,1,sizeof(buf),temp);
			i--;
	}
	fclose(temp);
	*/
	FILE *f= fopen("/Volumes/os-10/randomData.dat","r");
	setvbuf( f, (char *)NULL, _IONBF, 0 ); 
	
	if(sequential == 0)
	{
		for(int j = 0;j<repetitions;j++)
		{
			i = size;
			asm volatile ("CPUID\n\t"
	                      "RDTSC\n\t"
	                      "mov %%edx, %0\n\t"
	                      "mov %%eax, %1\n\t": "=r" (cycles_high), "=r" (cycles_low)::
	                      "%rax", "%rbx", "%rcx", "%rdx");

			for(int k =0;k<noReads;k++)
			{
				int position = rand()%size;
				fseek(f,position*blockSize,SEEK_SET);
				fread(buf,blockSize,1,f);
			}
			
			asm volatile("RDTSCP\n\t"
		     "mov %%edx, %0\n\t" "mov %%eax, %1\n\t"
	             "CPUID\n\t": "=r" (cycles_high1), "=r" (cycles_low1)::
	             "%rax", "%rbx", "%rcx", "%rdx");

			uint64_t end = ( ((uint64_t)cycles_high1 << 32) | cycles_low1 );
			uint64_t start = ( ((uint64_t)cycles_high << 32) | cycles_low );

			cout<<size<<","<<sequential<<","<<float(end-start)<<endl;
		}

	}
	else
	{
		for(int j = 0;j<repetitions;j++)
		{
			i = size;
			int position = rand()%size;
			fseek(f,position*blockSize,SEEK_SET);

			asm volatile ("CPUID\n\t"
	                      "RDTSC\n\t"
	                      "mov %%edx, %0\n\t"
	                      "mov %%eax, %1\n\t": "=r" (cycles_high), "=r" (cycles_low)::
	                      "%rax", "%rbx", "%rcx", "%rdx");

			for(int k =0;k<noReads;k++)
			{
				fread(buf,blockSize,1,f);
			}

			asm volatile("RDTSCP\n\t"
		     "mov %%edx, %0\n\t" "mov %%eax, %1\n\t"
	             "CPUID\n\t": "=r" (cycles_high1), "=r" (cycles_low1)::
	             "%rax", "%rbx", "%rcx", "%rdx");

			uint64_t end = ( ((uint64_t)cycles_high1 << 32) | cycles_low1 );
			uint64_t start = ( ((uint64_t)cycles_high << 32) | cycles_low );

			cout<<size<<","<<sequential<<","<<float(end-start)<<endl;
		}
	}

	fclose(f);

	
}
