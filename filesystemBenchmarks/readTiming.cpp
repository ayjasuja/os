#include<iostream>
#include<fstream>
#include<vector>
#include<algorithm>

using namespace std;
unsigned cycles_high,cycles_low,cycles_low1,cycles_high1;


int main(int argc,char* argv[])
{

	int size= atoi(argv[1]); //Size input in megabytes
	int repetitions = atoi(argv[2]);

	
	const int numbytes = 1048576;
	FILE *fp = fopen("/dev/urandom","r");
	FILE *temp = fopen("temp.dat","wb");
	setvbuf( temp, (char *)NULL, _IONBF, 0 );

	int i = size;
	
	const int bufsize = size*numbytes;
	char buf[bufsize];
	
	//while (i>0)
	//{
			size_t len = fread(buf, sizeof(buf), 1, fp);
			fwrite(buf, sizeof(buf), 1, temp);
	//		i--;
	//}

	fdatasync(temp);
	fclose(temp);
	

	FILE *temp = fopen("temp.dat","wb");
	setvbuf( temp, (char *)NULL, _IONBF, 0 );

	for(int j = 0;j<repetitions;j++)
	{
		i = size;
		asm volatile ("CPUID\n\t"
	                      "RDTSC\n\t"
	                      "mov %%edx, %0\n\t"
	                      "mov %%eax, %1\n\t": "=r" (cycles_high), "=r" (cycles_low)::
	                      "%rax", "%rbx", "%rcx", "%rdx");

		//while(i>0)
		//{
		size_t len = fread(buf, sizeof(buf), 1, temp);
		//	i--;
		//}

		asm volatile("RDTSCP\n\t"
		     "mov %%edx, %0\n\t" "mov %%eax, %1\n\t"
	             "CPUID\n\t": "=r" (cycles_high1), "=r" (cycles_low1)::
	             "%rax", "%rbx", "%rcx", "%rdx");

		
		uint64_t end = ( ((uint64_t)cycles_high1 << 32) | cycles_low1 );
		uint64_t start = ( ((uint64_t)cycles_high << 32) | cycles_low );

		cout<<size<<","<<float(end-start)<<'\n';
	}
	fclose(fp);
	fclose(temp);
	return 0;
}

	
