#include<iostream>
#include<fstream>
#include<vector>
#include<algorithm>
#include<unistd.h>
#include<cstdlib>

using namespace std;
unsigned cycles_high,cycles_low,cycles_low1,cycles_high1;


int main(int argc,char* argv[])
{

	const int size= atoi(argv[1]); //Size input in megabytes
	const long int filesize = size*1049576;
	int repetitions = atoi(argv[2]);

	
	FILE *fp = fopen("/dev/urandom","r");
	FILE *temp = fopen("temp.dat","wb");
    	setvbuf( temp, (char *)NULL, _IONBF, 0 );
    
	int i = size;
	char *buf = new char[filesize];
    	
	//while (i>0)
	//{
	fread(buf,filesize,1,fp);
	fwrite(buf,filesize,1,temp);
		//i--;
	//}
	
	//fdatasync(temp);
	fclose(temp);
	fclose(fp);
	

	temp = fopen("temp.dat","r");
	//setvbuf( temp, (char *)NULL, _IONBF, 0 );

	for(int j = 0;j<repetitions;j++)
	{
		i = size;
		asm volatile ("CPUID\n\t"
	                      "RDTSC\n\t"
	                      "mov %%edx, %0\n\t"
	                      "mov %%eax, %1\n\t": "=r" (cycles_high), "=r" (cycles_low)::
	                      "%rax", "%rbx", "%rcx", "%rdx");

		
		/*while (i>0)
		{
			size_t len = fread(buf, filesize, 1, temp);
			i--;
		}*/
		fread(buf, filesize, 1, temp);
		asm volatile("RDTSCP\n\t"
		     "mov %%edx, %0\n\t" "mov %%eax, %1\n\t"
	             "CPUID\n\t": "=r" (cycles_high1), "=r" (cycles_low1)::
	             "%rax", "%rbx", "%rcx", "%rdx");

		
		uint64_t end = ( ((uint64_t)cycles_high1 << 32) | cycles_low1 );
		uint64_t start = ( ((uint64_t)cycles_high << 32) | cycles_low );

		cout<<size<<","<<float(end-start)<<'\n';
	}
	return 0;
}

	
