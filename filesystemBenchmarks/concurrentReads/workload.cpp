#include<iostream>
#include<cstdio>
#include<unistd.h>
#include<string>
#include<csignal>
#include<cstdlib>
#include<cstdio>

using namespace std;

unsigned cycles_high,cycles_low,cycles_low1,cycles_high1;

void terminateHandler(int signum)
{
	cout << "Interrupt signal (" << signum << ") received.\n";
	exit(signum);
}

int main(int argc,char* argv[])
{
	const int numberCompetitors = atoi(argv[1]);
	
	int id = 0,status,options,offset;
	pid_t pids[numberCompetitors],parent= getpid();
	
	const int size = 1;
	const int numbytes = 4*1048576;
	const int repetitions = 10;
	
	FILE *temp = fopen("/dev/rdisk2s2","r");
	setvbuf( temp, (char *)NULL, _IONBF, 0 );

	for (int i = 0; i < numberCompetitors; ++i) 
	{
  		if ((pids[i] = fork()) < 0) 
  		{
    		perror("fork");
    		abort();
  		} 
  		else if (pids[i] == 0) 
  		{
    	
    		usleep(1000);

			/*FILE *fp = fopen("/dev/urandom","r");
			FILE *temp = tmpfile();
			setvbuf( temp, (char *)NULL, _IONBF, 0 );

			int i = size;*/
			
			const int bufsize = numbytes;
			char *buf = new char[numbytes];
			
			/*size_t len = fread(buf, sizeof(buf), 1, fp);
			fwrite(buf, sizeof(buf), 1, temp);
			
			fdatasync(temp);
			fclose(temp);*/
			offset = rand()%size;
			fseek(temp,size*offset,SEEK_SET);

			for(int j = 0;j<repetitions;j++)
			{
				asm volatile ("CPUID\n\t"
			                      "RDTSC\n\t"
			                      "mov %%edx, %0\n\t"
			                      "mov %%eax, %1\n\t": "=r" (cycles_high), "=r" (cycles_low)::
			                      "%rax", "%rbx", "%rcx", "%rdx");

				
					size_t len = fread(buf, bufsize, 1, temp);
			

				asm volatile("RDTSCP\n\t"
				     "mov %%edx, %0\n\t" "mov %%eax, %1\n\t"
			             "CPUID\n\t": "=r" (cycles_high1), "=r" (cycles_low1)::
			             "%rax", "%rbx", "%rcx", "%rdx");

				
				uint64_t end = ( ((uint64_t)cycles_high1 << 32) | cycles_low1 );
				uint64_t start = ( ((uint64_t)cycles_high << 32) | cycles_low );

				cout<<numberCompetitors<<","<<float(end-start)<<'\n';
			}
    		exit(0);
  		}
	}
	/*for(int k =0;k<numberCompetitors;k++)
	{
		offset = k;
		children_pid[k]= fork();
		if(children_pid[k] == -1)
		{
			cout<<"Fork failed"<<endl;
			exit(1);
		}
		if(getpid() != parent)
		{
			break;
		}
	}*/

	
	/* Wait for children to exit. */
	int n=numberCompetitors;
	pid_t ret_pid;
	while (n > 0) 
	{
  		ret_pid = wait(&status);
  		//printf("Child with PID %ld exited with status 0x%x.\n", (long)ret_pid, status);
  		--n;  
	}

	//return(0);
}
