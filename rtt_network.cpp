#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h> 
#include <vector>
#include <algorithm>
#include <iostream>
using namespace std;


// ncat -l 7 --keep-open --exec "/bin/cat"

void error(const char *msg)
{
    perror(msg);
    exit(0);
}

int main(int argc, char *argv[])
{
    int sockfd, portno, n;
    int bsize = 100;	
    struct sockaddr_in serv_addr;
    struct hostent *server;
    unsigned cycles_high, cycles_highl, cycles_low, cycles_lowl;
    vector<double> abc;
    double avg;
    long i = 0;
    int loop = 50;
    int x;
    uint64_t start,end, temp;

    if (argc < 3) {
       fprintf(stderr,"usage %s hostname port\n", argv[0]);
       exit(0);
    }
    portno = atoi(argv[2]);
    

    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0) 
        error("ERROR opening socket");
    server = gethostbyname(argv[1]);
    if (server == NULL) {
        fprintf(stderr,"ERROR, no such host\n");
        exit(0);
    }
    bzero((char *) &serv_addr, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    bcopy((char *)server->h_addr, 
         (char *)&serv_addr.sin_addr.s_addr,
         server->h_length);
    serv_addr.sin_port = htons(portno);

    for(int k=0;k<2000;k = k + 10)	
    {
	    avg = 0;	
	    abc.clear();
	    bsize = 100*(k+1);		
	    char buffer[bsize];	
	    for(i=0;i<bsize;i++)
		buffer[i] = '1'	;
	    buffer[bsize] = '\0';
	    //cout << strlen(buffer) << "\n";
	    for(i=0;i<loop;i++)	
	    {	
		    temp = 0;
		    sockfd = socket(AF_INET, SOCK_STREAM, 0);
		    asm volatile ("CPUID\n\t"
				        "RDTSC\n\t"
				        "mov %%edx, %0\n\t"
				        "mov %%eax, %1\n\t" : "=r" (cycles_high), "=r" (cycles_low) :: "%rax", "%rbx", "%rcx", "%rdx");
		    if (connect(sockfd,(struct sockaddr *) &serv_addr,sizeof(serv_addr)) < 0) 
			error("ERROR connecting");
		    n = write(sockfd,buffer,strlen(buffer));
		    //if (n < 0) 
		    //     error("ERROR writing to socket");
		    n = read(sockfd,buffer,255);
		    //if (n < 0) 
		    //     error("ERROR reading from socket");
		    //printf("%s\n",buffer);
		    close(sockfd);
		    asm volatile ("RDTSCP\n\t"
				        "mov %%edx, %0\n\t"
				        "mov %%eax, %1\n\t"
				        "CPUID\n\t": "=r" (cycles_highl), "=r" (cycles_lowl):: "%rax", "%rbx", "%rcx", "%rdx");

		    
		    if (n < 0) 
			 error("ERROR reading from socket");

		    start = (((uint64_t)cycles_high << 32) | cycles_low);
		    end = (((uint64_t)cycles_highl << 32) | cycles_lowl);
		    temp = (end - start);
		    //cout << temp << "\n"; 
		    abc.push_back(temp);	
	    }
	    sort(abc.begin(), abc.end());	
	    for(int x=5;x<loop-5;x++)
		avg = avg + abc[x]/(loop-10);
	    cout << bsize << "," << avg  << ","<<  abc[loop/2] <<"\n";
    }
    return 0;
}

/* 
 ------ Results - Remote ----------------------

Bytes : 100 Avg 8.36946e+07 Median : 8.36984e+07
Bytes : 200 Avg 8.35676e+07 Median : 8.36657e+07
Bytes : 300 Avg 4.87872e+07 Median : 3.00504e+07
Bytes : 400 Avg 8.39071e+07 Median : 8.38592e+07
Bytes : 500 Avg 8.38932e+07 Median : 8.39179e+07
Bytes : 600 Avg 8.40302e+07 Median : 8.40372e+07
Bytes : 700 Avg 8.40641e+07 Median : 8.40987e+07
Bytes : 800 Avg 8.39916e+07 Median : 8.40022e+07
Bytes : 900 Avg 8.03207e+07 Median : 8.40838e+07
Bytes : 1000 Avg 8.2819e+07 Median : 8.38212e+07
Bytes : 1100 Avg 8.36003e+07 Median : 8.36128e+07
Bytes : 1200 Avg 8.34122e+07 Median : 8.36908e+07
Bytes : 1300 Avg 6.33072e+07 Median : 7.3214e+07
Bytes : 1400 Avg 8.43516e+07 Median : 8.43637e+07
Bytes : 1500 Avg 8.49169e+07 Median : 8.50557e+07
Bytes : 1600 Avg 8.4694e+07 Median : 8.47851e+07
Bytes : 1700 Avg 8.40864e+07 Median : 8.46167e+07
Bytes : 1800 Avg 8.50632e+07 Median : 8.49993e+07
Bytes : 1900 Avg 8.53207e+07 Median : 8.53264e+07
Bytes : 2000 Avg 7.96016e+07 Median : 8.48396e+07

------------ Results - Localhost ---------------

Bytes : 100 Avg 1.8994e+06 Median : 1.83509e+06
Bytes : 200 Avg 1.6761e+06 Median : 1.63772e+06
Bytes : 300 Avg 1.64527e+06 Median : 1.61517e+06
Bytes : 400 Avg 1.70963e+06 Median : 1.67865e+06
Bytes : 500 Avg 1.68666e+06 Median : 1.65362e+06
Bytes : 600 Avg 1.68006e+06 Median : 1.63884e+06
Bytes : 700 Avg 1.66441e+06 Median : 1.64686e+06
Bytes : 800 Avg 1.72875e+06 Median : 1.72559e+06
Bytes : 900 Avg 1.66974e+06 Median : 1.65092e+06
Bytes : 1000 Avg 1.67604e+06 Median : 1.64974e+06
Bytes : 1100 Avg 1.6695e+06 Median : 1.63585e+06
Bytes : 1200 Avg 1.65635e+06 Median : 1.6232e+06
Bytes : 1300 Avg 1.68763e+06 Median : 1.63609e+06
Bytes : 1400 Avg 1.71521e+06 Median : 1.6721e+06
Bytes : 1500 Avg 1.67863e+06 Median : 1.64936e+06
Bytes : 1600 Avg 1.65208e+06 Median : 1.63111e+06
Bytes : 1700 Avg 1.67764e+06 Median : 1.65727e+06
Bytes : 1800 Avg 1.64582e+06 Median : 1.6111e+06
Bytes : 1900 Avg 1.67645e+06 Median : 1.65361e+06
Bytes : 2000 Avg 1.67743e+06 Median : 1.66691e+06

-------------------- ICMP- Remote ---------------
100  rtt min/avg/max/mdev = 0.991/1.113/1.474/0.086 ms
200  rtt min/avg/max/mdev = 0.789/1.154/1.444/0.070 ms
300  rtt min/avg/max/mdev = 1.028/1.161/1.667/0.077 ms
400  rtt min/avg/max/mdev = 0.957/1.181/1.526/0.063 ms
500  rtt min/avg/max/mdev = 0.893/1.185/1.457/0.067 ms
600  rtt min/avg/max/mdev = 0.971/1.140/1.349/0.066 ms
700  rtt min/avg/max/mdev = 0.961/1.148/1.449/0.070 ms
800  rtt min/avg/max/mdev = 0.991/1.139/1.492/0.078 ms
900  rtt min/avg/max/mdev = 0.961/1.165/1.444/0.071 ms
1000 rtt min/avg/max/mdev = 0.987/1.194/1.374/0.067 ms
1100 rtt min/avg/max/mdev = 1.006/1.176/1.423/0.077 ms
1200 rtt min/avg/max/mdev = 0.989/1.141/1.600/0.084 ms
1300 rtt min/avg/max/mdev = 0.987/1.136/1.406/0.059 ms
1400 rtt min/avg/max/mdev = 0.807/1.170/1.447/0.083 ms
1500 rtt min/avg/max/mdev = 1.196/1.383/1.634/0.075 ms
1600 rtt min/avg/max/mdev = 1.326/1.534/2.031/0.084 ms
1700 rtt min/avg/max/mdev = 1.077/1.544/1.860/0.081 ms
1800 rtt min/avg/max/mdev = 1.343/1.558/2. 016/0.097 ms
1900 rtt min/avg/max/mdev = 1.349/1.562/1.813/0.094 ms
2000 rtt min/avg/max/mdev = 1.383/1.549/1.785/0.076 ms

-------------------- ICMP- Local ---------------
100 rtt min/avg/max/mdev = 0.033/0.068/0.150/0.018 ms
200 rtt min/avg/max/mdev = 0.033/0.066/0.112/0.019 ms
300 rtt min/avg/max/mdev = 0.035/0.066/0.132/0.019 ms
400 rtt min/avg/max/mdev = 0.033/0.064/0.132/0.018 ms
500 rtt min/avg/max/mdev = 0.034/0.065/0.109/0.016 ms
600 rtt min/avg/max/mdev = 0.028/0.062/0.101/0.017 ms
700 rtt min/avg/max/mdev = 0.032/0.066/0.112/0.019 ms
800 rtt min/avg/max/mdev = 0.035/0.065/0.134/0.016 ms
900 rtt min/avg/max/mdev = 0.028/0.064/0.115/0.019 ms
1000 rtt min/avg/max/mdev = 0.033/0.068/0.122/0.017 ms
1100 rtt min/avg/max/mdev = 0.035/0.071/0.128/0.020 ms
1200 rtt min/avg/max/mdev = 0.034/0.067/0.121/0.016 ms
1300 rtt min/avg/max/mdev = 0.022/0.068/0.115/0.018 ms
1400 rtt min/avg/max/mdev = 0.027/0.069/0.125/0.020 ms
1500 rtt min/avg/max/mdev = 0.040/0.068/0.126/0.015 ms
1600 rtt min/avg/max/mdev = 0.034/0.067/0.122/0.020 ms
1700 rtt min/avg/max/mdev = 0.034/0.069/0.136/0.019 ms
1800 rtt min/avg/max/mdev = 0.035/0.072/0.147/0.017 ms
1900 rtt min/avg/max/mdev = 0.035/0.069/0.160/0.021 ms
2000 rtt min/avg/max/mdev = 0.037/0.070/0.121/0.018 ms
*/

