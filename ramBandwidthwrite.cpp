#include <chrono>
#include <iostream>
#include <cstdio>
#include <cmath>
#include <sys/types.h>
#include <unistd.h>
#include <sys/time.h>
#include <vector>
#include <stdlib.h>
#include <algorithm>
#include <signal.h>
#include <sys/types.h>
#include <sys/resource.h>
using namespace std;

int main()
{
	int size = 2000000;
	long addforloopoverhead = 0,measurementoverhead = 41;
	long median;
	unsigned cycles_high, cycles_highl, cycles_low, cycles_lowl;
	uint64_t start,end, temp;
	struct timeval tv;
	vector<long> abc;
	long l,i = 0;
	int array[size];
	int dummy[5000];
	int x;
	int k,add;

	for(k=0;k<size;k=k+1)
		{
			array[k] = 1;
		}
	
	for(i=0;i<501;i++)
	{
		temp = 0;
		asm volatile ("CPUID\n\t"
		"RDTSC\n\t"
		"mov %%edx, %0\n\t"
		"mov %%eax, %1\n\t" : "=r" (cycles_high), "=r" (cycles_low) :: "%rax", "%rbx", "%rcx", "%rdx");

		/*operation time
		for(k=0;k<size;k=k+5)
		{
			array[k] = i;
			array[k+1] = i;
			array[k+2] = i;
			array[k+3] = i;
			array[k+4] = i;
		}*/

		// for loop overhead
		for(k=0;k<size;k=k+5)
		{
			add = 0;
			add = add + 1;
			add = add + 2;
			add = add + 3;
			add = add + 4;
		}

		asm volatile ("RDTSCP\n\t"
		"mov %%edx, %0\n\t"
		"mov %%eax, %1\n\t"
		"CPUID\n\t": "=r" (cycles_highl), "=r" (cycles_lowl):: "%rax", "%rbx", "%rcx", "%rdx");

		start = (((uint64_t)cycles_high << 32) | cycles_low);
		end = (((uint64_t)cycles_highl << 32) | cycles_lowl);
		temp += end - start;
		//cout << temp << '\n';
		abc.push_back(temp);

		l = 0;
		for(k=0;k<size;k++)
		{
			l += array[k];
		}
	}
	
	sort(abc.begin(), abc.end());
	median =  (abc[250]) - addforloopoverhead - measurementoverhead;
	cout << "Median cycle value (write bandwidth) : " << median << "\n";
	return 0;
}
