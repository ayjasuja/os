#include <chrono>
#include <iostream>
#include <cstdio>
#include <cmath>
#include <sys/types.h>
#include <unistd.h>
#include <sys/time.h>
#include <vector>
#include <stdlib.h>
#include <algorithm>
#include <signal.h>
#include <sys/types.h>
#include <sys/resource.h>
using namespace std;

int main()
{
	int size = 2000000;
	int addforloopoverhead,measurementoverhead = 41;
	unsigned cycles_high, cycles_highl, cycles_low, cycles_lowl;
	uint64_t start,end, temp;
	struct timeval tv;
	vector<long> abc;
	long l,i = 0;
	int array[size];
	int dummy[1000];
	int x;
	int k,garbage=911;
	for(k =0;k<size;k++)
	{
		array[k] = k;	
	}

	for(k=0;k<501;k++)
	{
		temp = 0;
		asm volatile ("CPUID\n\t"
		"RDTSC\n\t"
		"mov %%edx, %0\n\t"
		"mov %%eax, %1\n\t" : "=r" (cycles_high), "=r" (cycles_low) :: "%rax", "%rbx", "%rcx", "%rdx");

		for(i=0;i<size;i = i+5)
		{
			dummy[i%1000] = array[i] + garbage;
			dummy[(i+1)%1000] = array[i+1] + garbage;
			dummy[(i+2)%1000] = array[i+2] + garbage;
			dummy[(i+3)%1000] = array[i+3] + garbage;
			dummy[(i+4)%1000] = array[i+4] + garbage;			
		}

		asm volatile ("RDTSCP\n\t"
		"mov %%edx, %0\n\t"
		"mov %%eax, %1\n\t"
		"CPUID\n\t": "=r" (cycles_highl), "=r" (cycles_lowl):: "%rax", "%rbx", "%rcx", "%rdx");

		start = (((uint64_t)cycles_high << 32) | cycles_low);
		end = (((uint64_t)cycles_highl << 32) | cycles_lowl);
		temp += end - start;
		abc.push_back(temp);

		l = 0;
		for(i=0;i<size;i++)
		{
			l += dummy[i%1000];
		}
	}
	
	sort(abc.begin(), abc.end());
	cout << "Median cycle value (read overhead): " << (abc[250] - measurementoverhead) << "\n";
	return 0;
}
