#include <chrono>
#include <iostream>
#include <cstdio>
#include <cmath>
#include <sys/types.h>
#include <unistd.h>
#include <sys/time.h>
#include <vector>
#include <algorithm>
#include <signal.h>
#include <stdlib.h> 
#include <sys/resource.h>
using namespace std;

int main()
{
	uint64_t mOverhead = 22;
	struct rlimit rl;
    	getrlimit(RLIMIT_NPROC, &rl);
	unsigned cycles_high, cycles_highl, cycles_low, cycles_lowl;
	vector<double> abc;
	long i = 0;
	int x;
	pid_t pid;
	uint64_t start,end, temp;
	struct timeval *tv;
	
	for(int k=0;k<51;k++)
	{
		temp = 0;
		for(i=0;i<100;i++)
		{
			 asm volatile ("CPUID\n\t"
                        "RDTSC\n\t"
                        "mov %%edx, %0\n\t"
                        "mov %%eax, %1\n\t" : "=r" (cycles_high), "=r" (cycles_low) :: "%rax", "%rbx", "%rcx", "%rdx");

			pid = fork();

			asm volatile ("RDTSCP\n\t"
                        "mov %%edx, %0\n\t"
                        "mov %%eax, %1\n\t"
                        "CPUID\n\t": "=r" (cycles_highl), "=r" (cycles_lowl):: "%rax", "%rbx", "%rcx", "%rdx");

			if (pid == 0)
    			{
				exit(0);
    			}
    			else if (pid > 0)
    			{
        			// parent process
        		}
    			else
    			{
        			printf("fork() failed!\n");
				exit(0);
    			}

			start = (((uint64_t)cycles_high << 32) | cycles_low);
                        end = (((uint64_t)cycles_highl << 32) | cycles_lowl);
                        temp += end - start;
	
		}
                abc.push_back(temp);
	}
	sort(abc.begin(), abc.end());
	cout << "Median cycle Value : " << (abc[25]/100 - mOverhead)<< "\n";
	return 0;
}
