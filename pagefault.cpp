#include <chrono>
#include <iostream>
#include <cstdio>
#include <cmath>
#include <sys/types.h>
#include <unistd.h>
#include <sys/time.h>
#include <vector>
#include <stdlib.h>
#include <algorithm>
#include <signal.h>
#include <sys/types.h>
#include <sys/resource.h>
#include <sys/mman.h>
#include <err.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
using namespace std;

int main()
{
	int size = 200;
	long addforloopoverhead = 0,measurementoverhead = 41;
	long median;
	unsigned cycles_high, cycles_highl, cycles_low, cycles_lowl;
	uint64_t start,end, temp;
	struct timeval tv;
	vector<long> abc;
	long l,i = 0;
	int x;
	int k;
	const char str1[] = "string 1";
	char *anon;
	cout << sysconf(_SC_PAGE_SIZE) << "\n";
	for(k=0;k<10001;k++)
	{
		temp = 0;
		asm volatile ("CPUID\n\t"
		"RDTSC\n\t"
		"mov %%edx, %0\n\t"
		"mov %%eax, %1\n\t" : "=r" (cycles_high), "=r" (cycles_low) :: "%rax", "%rbx", "%rcx", "%rdx");

		anon = (char*)mmap(NULL, 4096, PROT_READ|PROT_WRITE, MAP_ANON|MAP_SHARED, -1, 0);
		
		asm volatile ("RDTSCP\n\t"
		"mov %%edx, %0\n\t"
		"mov %%eax, %1\n\t"
		"CPUID\n\t": "=r" (cycles_highl), "=r" (cycles_lowl):: "%rax", "%rbx", "%rcx", "%rdx");
		
		//cout << k << '\n';
		strcpy(anon, str1);
		munmap(anon, 4096);

		start = (((uint64_t)cycles_high << 32) | cycles_low);
		end = (((uint64_t)cycles_highl << 32) | cycles_lowl);
		temp += end - start;
		//cout << temp << '\n';
		abc.push_back(temp);

		l = 0;
		for(i=0;i<1000;i++)
		{
			//l += dummy[i];
		}
	}
	
	sort(abc.begin(), abc.end());
	median =  (abc[5001]) - measurementoverhead;
	cout << "Median cycle value (page fault overhead): " << median << "\n";
	return 0;
}
