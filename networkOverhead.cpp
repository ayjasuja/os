#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h> 
#include <vector>
#include <algorithm>
#include <iostream>
#include <errno.h>
#include <netinet/tcp.h>	
using namespace std;


// ncat -l 7 --keep-open --recv-only

void error(const char *msg)
{
    perror(msg);
    exit(0);
}

int main(int argc, char *argv[])
{
    int sockfd, portno, n;
    int bsize = 12500000;	
    struct sockaddr_in serv_addr;
    struct hostent *server;
    unsigned cycles_high, cycles_highl, cycles_low, cycles_lowl;
    vector<double> abc, clo;
    double avg, cavg;
    long i = 0;
    int loop = 100;
    int x;
    uint64_t start,end, temp;
    int optval = 1;

    if (argc < 3) {
       fprintf(stderr,"usage %s hostname port\n", argv[0]);
       exit(0);
    }
    portno = atoi(argv[2]);
    

    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0) 
        error("ERROR opening socket");
    server = gethostbyname(argv[1]);
    if (server == NULL) {
        fprintf(stderr,"ERROR, no such host\n");
        exit(0);
    }
    bzero((char *) &serv_addr, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    bcopy((char *)server->h_addr, 
         (char *)&serv_addr.sin_addr.s_addr,
         server->h_length);
    serv_addr.sin_port = htons(portno);

    avg = 0;
    cavg = 0;	
    abc.clear();	
    char *buffer;	
    buffer = (char*)malloc(bsize*sizeof(char));
    for(i=0;i<bsize;i++)
	buffer[i] = '1'	;
    buffer[bsize] = '\0';
    //cout << strlen(buffer) << "\n";
    for(i=0;i<loop;i++)	
    {	
	    temp = 0;
	    asm volatile ("CPUID\n\t"
			        "RDTSC\n\t"
			        "mov %%edx, %0\n\t"
			        "mov %%eax, %1\n\t" : "=r" (cycles_high), "=r" (cycles_low) :: "%rax", "%rbx", "%rcx", "%rdx");

	    sockfd = socket(AF_INET, SOCK_STREAM, 0);
	    if (connect(sockfd,(struct sockaddr *) &serv_addr,sizeof(serv_addr)) < 0) 
		error("ERROR connecting");

	    asm volatile ("RDTSCP\n\t"
			        "mov %%edx, %0\n\t"
			        "mov %%eax, %1\n\t"
			        "CPUID\n\t": "=r" (cycles_highl), "=r" (cycles_lowl):: "%rax", "%rbx", "%rcx", "%rdx");
	    start = (((uint64_t)cycles_high << 32) | cycles_low);
	    end = (((uint64_t)cycles_highl << 32) | cycles_lowl);
	    temp = (end - start);
	    abc.push_back(temp);	

	    asm volatile ("CPUID\n\t"
			        "RDTSC\n\t"
			        "mov %%edx, %0\n\t"
			        "mov %%eax, %1\n\t" : "=r" (cycles_high), "=r" (cycles_low) :: "%rax", "%rbx", "%rcx", "%rdx");
	    close(sockfd);	
	    asm volatile ("RDTSCP\n\t"
			        "mov %%edx, %0\n\t"
			        "mov %%eax, %1\n\t"
			        "CPUID\n\t": "=r" (cycles_highl), "=r" (cycles_lowl):: "%rax", "%rbx", "%rcx", "%rdx");	
	    start = (((uint64_t)cycles_high << 32) | cycles_low);
	    end = (((uint64_t)cycles_highl << 32) | cycles_lowl);
	    temp = (end - start);
	    clo.push_back(temp);	
    }
    sort(abc.begin(), abc.end());	
    sort(clo.begin(), clo.end());	
    for(int x=0;x<loop;x++)
    {
	avg = avg + abc[x]/(loop);
	cavg = cavg + clo[x]/(loop);
    }
    cout << "Connection --> " << " Avg " << avg  << " Median : "<<  abc[loop/2] <<"\n";
    cout << "Close --> " << " Avg " << cavg  << " Median : "<<  clo[loop/2] <<"\n";
    return 0;
}

/* 
 ------ Results - Remote ------
Connection -->  Avg 2.68663e+07 Median : 1.24529e+06
Close -->  Avg 55975.2 Median : 42894

 ------ Results - Local ------
Connection -->  Avg 87665.1 Median : 74960
Close -->  Avg 31135.5 Median : 28720
*/

