#include<iostream>
#include<cstdio>
#include <unistd.h>

using namespace std;

unsigned cycles_high,cycles_low,cycles_low1,cycles_high1;

__attribute__((noinline)) void clearCache(int a, int b, int c, int d, int e, int f, int g)
{
//	int a = 1+1;
	return;
}

int  main()
{
	//benchmarking code
	for(int j=0;j<100;j++)
	{
		usleep(100000);
		asm volatile ("CPUID\n\t" "RDTSC\n\t"
                                       "mov %%edx, %0\n\t"
                                       "mov %%eax, %1\n\t": "=r" (cycles_high), "=r" (cycles_low)::
                              "%rax", "%rbx", "%rcx", "%rdx");

		for(int i=0;i<20000;i++)
		{
			clearCache(1,2,3,4,5,6,7);
		}

		asm volatile("RDTSCP\n\t"
                                "mov %%edx, %0\n\t" "mov %%eax, %1\n\t"
                                "CPUID\n\t": "=r" (cycles_high1), "=r" (cycles_low1)::
                             "%rax", "%rbx", "%rcx", "%rdx");
        	uint64_t end = ( ((uint64_t)cycles_high1 << 32) | cycles_low1 );
		uint64_t start = ( ((uint64_t)cycles_high << 32) | cycles_low );
		cout<<end-start<<'\n';
	}
	
}
