#include <chrono>
#include <iostream>
#include <cstdio>
#include <cmath>
#include <sys/types.h>
#include <unistd.h>
#include <sys/time.h>
#include <vector>
#include <stdlib.h>
#include <algorithm>
#include <signal.h>
#include <sys/types.h>
#include <sys/resource.h>
using namespace std;

int main()
{
	int size = 2000000;
	long addforloopoverhead = 0,measurementoverhead = 41;
	long median;
	unsigned cycles_high, cycles_highl, cycles_low, cycles_lowl;
	uint64_t start,end, temp;
	struct timeval tv;
	vector<long> abc;
	long l,i = 0;
	int array[size];
	int dummy[5000];
	int x;
	int k;
	
	for(k =0;k<size;k++)
	{
		array[k] = k;	
	}

	for(k=0;k<501;k++)
	{
		temp = 0;
		asm volatile ("CPUID\n\t"
		"RDTSC\n\t"
		"mov %%edx, %0\n\t"
		"mov %%eax, %1\n\t" : "=r" (cycles_high), "=r" (cycles_low) :: "%rax", "%rbx", "%rcx", "%rdx");

		for(i=0;i<size;i=i+5)
		{
			//simple loop unrolling
			dummy[i%1000] = array[i] + array[(i + size/2)%size];
			dummy[(i+1)%1000] = array[i+1] + array[(i + 1 + size/2)%size];
			dummy[(i+2)%1000] = array[i+2] + array[(i + 2 + size/2)%size];
			dummy[(i+3)%1000] = array[i+3] + array[(i + 3 + size/2)%size];
			dummy[(i+4)%1000] = array[i+4] + array[(i + 4 + size/2)%size];
		}

		asm volatile ("RDTSCP\n\t"
		"mov %%edx, %0\n\t"
		"mov %%eax, %1\n\t"
		"CPUID\n\t": "=r" (cycles_highl), "=r" (cycles_lowl):: "%rax", "%rbx", "%rcx", "%rdx");

		start = (((uint64_t)cycles_high << 32) | cycles_low);
		end = (((uint64_t)cycles_highl << 32) | cycles_lowl);
		temp += end - start;
		//cout << temp << '\n';
		abc.push_back(temp);

		l = 0;
		for(i=0;i<1000;i++)
		{
			l += dummy[i];
		}
	}
	
	sort(abc.begin(), abc.end());
	median =  (abc[250]) - addforloopoverhead - measurementoverhead;
	cout << "Median cycle value (read bandwidth): " << median << "\n";
	return 0;
}
