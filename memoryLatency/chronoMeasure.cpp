#include<iostream>
#include<cstdlib>
#include<unistd.h>
#include<chrono>

using namespace std;

unsigned cycles_high,cycles_low,cycles_low1,cycles_high1;

int main(int argc,char* argv[])
{
		
		using chrono::high_resolution_clock;

   		const long long int length = atoi(argv[1]);
		const long long int stride = atoi(argv[2]);
		const long long int count = atoi(argv[3]);
		
		long long int k =0;
		double sum = 0;
		
		long long int *a = (long long int*) malloc(length*sizeof(long long int));
		long long int index=0,dummy;
		
		for(;k<count;k++)
		{
			index = (index+stride)%length;

			high_resolution_clock::time_point start = high_resolution_clock::now();

			sum = sum+a[index];
		
			high_resolution_clock::time_point end = high_resolution_clock::now();


                	cout<<length<<","<<stride<<","<<count<<","<<float((end-start).count())/count<<'\n';
		}

}
