#include<iostream>
#include<cstdlib>
#include<unistd.h>

using namespace std;

unsigned cycles_high,cycles_low,cycles_low1,cycles_high1;

struct node
{
	int a;
	node *next;
};

int main(int argc,char* argv[])
{

const long long int length = atoi(argv[1]);
const long long int stride = atoi(argv[2]);
const long long int count = atoi(argv[3]);
long long int k =0;
double sum = 0;
node *a = new node[length];
long long int index=0,dummy;
//cout<<sizeof(node)<<endl;
//Initializing the node list
for(int i=0;i<length;i++)
{a[i].next = &a[(i+stride)%length];}

node *p = a;

asm volatile ("CPUID\n\t"
                      "RDTSC\n\t"
                      "mov %%edx, %0\n\t"
                      "mov %%eax, %1\n\t": "=r" (cycles_high), "=r" (cycles_low)::
                      "%rax", "%rbx", "%rcx", "%rdx");

for(k=0;k<count;k = k+1)
{
	p = p->next;
}
		
asm volatile("RDTSCP\n\t"
	     "mov %%edx, %0\n\t" "mov %%eax, %1\n\t"
             "CPUID\n\t": "=r" (cycles_high1), "=r" (cycles_low1)::
             "%rax", "%rbx", "%rcx", "%rdx");

uint64_t end = ( ((uint64_t)cycles_high1 << 32) | cycles_low1 );
uint64_t start = ( ((uint64_t)cycles_high << 32) | cycles_low );

cout<<length<<","<<stride<<","<<count<<","<<float(end-start)<<'\n';
delete[] a;

}
