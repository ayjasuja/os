#include<iostream>
#include<cstdlib>

using namespace std;

unsigned cycles_high,cycles_low,cycles_low1,cycles_high1;

int main(int argc,char* argv[])
{
	
		long long int stride = atoi(argv[1]);
		const long long int length = atoi(argv[2]);
		
		long double *a = new long double[length];
		
		for(long long int j=0;j<length;j=j+stride)
        	{
                	asm volatile ("CPUID\n\t" "RDTSC\n\t"
                        	               "mov %%edx, %0\n\t"
                                	       "mov %%eax, %1\n\t": "=r" (cycles_high), "=r" (cycles_low)::
 	                             	"%rax", "%rbx", "%rcx", "%rdx");

        	        a[j] = 1;

                	asm volatile("RDTSCP\n\t"
                        	        "mov %%edx, %0\n\t" "mov %%eax, %1\n\t"
                                	"CPUID\n\t": "=r" (cycles_high1), "=r" (cycles_low1)::
                             	     "%rax", "%rbx", "%rcx", "%rdx");
                	uint64_t end = ( ((uint64_t)cycles_high1 << 32) | cycles_low1 );
                	uint64_t start = ( ((uint64_t)cycles_high << 32) | cycles_low );
                	cout<<stride<<","<<end-start<<'\n';
        	}

	
}
