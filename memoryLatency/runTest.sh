g++ -O0 memoryLatency.cpp
for k in {32,64,128,256,512};do for i in {512,1024,2048,4096,8192,16384,32768,65536,131072,262144,524288,1048576,2097152,4194304,8388608,16777216}; do for j in $(seq 1 100);do ./a.out $i $k 100000 ;done;done;done>memoryLatency.csv

Rscript generatePlots.R
