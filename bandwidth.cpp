#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h> 
#include <vector>
#include <algorithm>
#include <iostream>
#include <errno.h>
#include <netinet/tcp.h>	
using namespace std;


// ncat -l 7000 --keep-open --recv-only > /dev/null

void error(const char *msg)
{
    perror(msg);
    exit(0);
}

int main(int argc, char *argv[])
{
    int sockfd, portno, n;
    int bsize = 10000000;	
    struct sockaddr_in serv_addr;
    struct hostent *server;
    unsigned cycles_high, cycles_highl, cycles_low, cycles_lowl;
    vector<double> abc;
    double avg;
    long i = 0;
    int loop = 20;
    int x;
    uint64_t start,end, temp;
    int optval = 1;

    if (argc < 3) {
       fprintf(stderr,"usage %s hostname port\n", argv[0]);
       exit(0);
    }
    portno = atoi(argv[2]);
    

    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0) 
        error("ERROR opening socket");
    server = gethostbyname(argv[1]);
    if (server == NULL) {
        fprintf(stderr,"ERROR, no such host\n");
        exit(0);
    }
    bzero((char *) &serv_addr, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    bcopy((char *)server->h_addr, 
         (char *)&serv_addr.sin_addr.s_addr,
         server->h_length);
    serv_addr.sin_port = htons(portno);

    avg = 0;	
    abc.clear();	
    char *buffer;	
    buffer = (char*)malloc(bsize*sizeof(char));
    for(i=0;i<bsize;i++)
	buffer[i] = '1'	;
    buffer[bsize] = '\0';
    //cout << strlen(buffer) << "\n";
    for(i=0;i<loop;i++)	
    {	
	    temp = 0;
	    sockfd = socket(AF_INET, SOCK_STREAM, 0);
	    optval = 1;
	    if (setsockopt(sockfd, IPPROTO_TCP, TCP_NODELAY, &optval, sizeof(int)) < 0)
        		printf("Cannot set TCP_NODELAY option " "on send socket (%s)\n", strerror(errno));

	    if (connect(sockfd,(struct sockaddr *) &serv_addr,sizeof(serv_addr)) < 0) 
		error("ERROR connecting");
	    asm volatile ("CPUID\n\t"
			        "RDTSC\n\t"
			        "mov %%edx, %0\n\t"
			        "mov %%eax, %1\n\t" : "=r" (cycles_high), "=r" (cycles_low) :: "%rax", "%rbx", "%rcx", "%rdx");
	    n = write(sockfd,buffer,strlen(buffer));
	    //if (n < 0) 
	    //     error("ERROR writing to socket");
	    asm volatile ("RDTSCP\n\t"
			        "mov %%edx, %0\n\t"
			        "mov %%eax, %1\n\t"
			        "CPUID\n\t": "=r" (cycles_highl), "=r" (cycles_lowl):: "%rax", "%rbx", "%rcx", "%rdx");

	    
	    if (n < 0) 
		 error("ERROR reading from socket");
	    close(sockfd);	
	    start = (((uint64_t)cycles_high << 32) | cycles_low);
	    end = (((uint64_t)cycles_highl << 32) | cycles_lowl);
	    temp = (end - start);
	    //cout << temp << "\n"; 
	    abc.push_back(temp);	
    }
    sort(abc.begin(), abc.end());
    avg = abc[0];		
    for(int x=0;x<loop;x++)
	{
	 if (avg > abc[x])
		avg = abc[x];
	}
    cout << "Bytes : " << bsize << " min " << avg  << " Median : "<<  abc[loop/2] <<"\n";
    return 0;
}

/* 
------ Results - Remote ------
Bytes : 10000000 Avg 3.08278e+08 Median : 3.02383e+08
*/

